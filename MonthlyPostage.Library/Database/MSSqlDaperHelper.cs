﻿using MonthlyPostage.Library.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

namespace MonthlyPostage.Library.Database
{
    public class SqlConectModelConfig
    {
        public string DB_HOST { get; set; }
        //public string DB_PORT { get; set; }
        public string DB_DATABASE { get; set; }
        public string DB_USERNAME { get; set; }
        public string DB_PASSWORD { get; set; }
    }
    public class MSSqlDaperHelper : IDisposable
    {
        private readonly ConfigurationHelper _configurationHelper;
        private SqlConnection connection = null;
        private String connectionString = "Server={0};Database={1};Uid={2};Pwd={3};";

        public MSSqlDaperHelper(string keyConnnectionString)
        {
            try
            {
                _configurationHelper = new ConfigurationHelper();
                if (!string.IsNullOrEmpty(keyConnnectionString.Trim()))
                {
                    //connectionString = new ConfigurationHelper().config[$"ConnectionStrings:{keyConnnectionString}"];
                    SqlConectModelConfig sqlConectModelConfig = _configurationHelper.GetAppSettings<SqlConectModelConfig>($"ConnectionStrings:{keyConnnectionString}");
                    connectionString = string.Format(this.connectionString, sqlConectModelConfig.DB_HOST, sqlConectModelConfig.DB_DATABASE, sqlConectModelConfig.DB_USERNAME, sqlConectModelConfig.DB_PASSWORD);
                }
                else { connectionString = ""; }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IDbConnection CreateConnection()
        {
            if (connection != null)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            connection = new SqlConnection(connectionString);

            return connection;
        }


        #region IDisposable Members
        public void Dispose()
        {
            if (connection != null)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Destructor
        ~MSSqlDaperHelper()
        {
            Dispose();
        }
        #endregion
    }

    //services.AddHangfire(configuration => configuration.UseRedisStorage("127.27.9.26:6379,AllowAdmin=true,abortConnect=false,SyncTimeout=5000"));
    public class HangFireRedisStorage
    {
        public string DB_HOST { get; set; }
        public string DB_PORT { get; set; }
        public int SyncTimeout { get; set; }
    }


}
