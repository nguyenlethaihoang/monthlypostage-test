﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data.Common;
using System.Xml;
using MonthlyPostage.Library.Configuration;

namespace MonthlyPostage.Library.Database
{
    public class SqlHelper
    {
        public static string connectionString = new ConfigurationHelper().config["ConnectionStrings:SQLConnectionRead"];
        private static int CommandTimeOut = 15;

        public static ConfigurationHelper _configurationHelper;

        private SqlConnection connection = null;

        public static string ConnRead()
        {
            _configurationHelper = new ConfigurationHelper();
            String connectionStringF = "Server={0};Database={1};Uid={2};Pwd={3};";

            SqlConectModelConfig sqlConectModelConfig = _configurationHelper.GetAppSettings<SqlConectModelConfig>($"ConnectionStrings:SQLConnectionRead");
            connectionStringF = string.Format(connectionStringF, sqlConectModelConfig.DB_HOST, sqlConectModelConfig.DB_DATABASE, sqlConectModelConfig.DB_USERNAME, sqlConectModelConfig.DB_PASSWORD);

            SqlHelper.connectionString = connectionStringF;
            return SqlHelper.connectionString;
        }

        public static string ConnWrite()
        {
            _configurationHelper = new ConfigurationHelper();
            String connectionStringF = "Server={0};Database={1};Uid={2};Pwd={3};";

            SqlConectModelConfig sqlConectModelConfig = _configurationHelper.GetAppSettings<SqlConectModelConfig>($"ConnectionStrings:SQLConnectionWrite");
            connectionStringF = string.Format(connectionStringF, sqlConectModelConfig.DB_HOST, sqlConectModelConfig.DB_DATABASE, sqlConectModelConfig.DB_USERNAME, sqlConectModelConfig.DB_PASSWORD);

            SqlHelper.connectionString = connectionStringF;
            return SqlHelper.connectionString;
        }

        private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            if (commandParameters == null)
                return;
            foreach (SqlParameter commandParameter in commandParameters)
            {
                if (commandParameter != null)
                {
                    if ((commandParameter.Direction == ParameterDirection.InputOutput || commandParameter.Direction == ParameterDirection.Input) && commandParameter.Value == null)
                        commandParameter.Value = (object)DBNull.Value;
                    command.Parameters.Add(commandParameter);
                }
            }
        }

        public static object CheckDateNull(object readerValue) => readerValue == DBNull.Value || readerValue == null ? (object)null : readerValue;

        public static int CheckIntNull(object readerValue) => readerValue == DBNull.Value || readerValue == null ? 0 : Convert.ToInt32(readerValue);

        public static string CheckStringNull(object readerValue) => readerValue == DBNull.Value || string.IsNullOrEmpty(readerValue.ToString()) ? "" : Convert.ToString(readerValue);

        public static float CheckFloatNull(object readerValue) => readerValue == DBNull.Value || readerValue == null ? -1f : Convert.ToSingle(readerValue);

        public static bool CheckBooleanNull(object readerValue) => readerValue != DBNull.Value && readerValue != null && Convert.ToBoolean(readerValue);

        private static void AssignParameterValues(SqlParameter[] commandParameters, DataRow dataRow)
        {
            if (commandParameters == null || dataRow == null)
                return;
            int num = 0;
            foreach (SqlParameter commandParameter in commandParameters)
            {
                if (commandParameter.ParameterName == null || commandParameter.ParameterName.Length <= 1)
                    throw new Exception(string.Format("Please provide a valid parameter name on the parameter #{0}, the ParameterName property has the following value: '{1}'.", (object)num, (object)commandParameter.ParameterName));
                if (dataRow.Table.Columns.IndexOf(commandParameter.ParameterName.Substring(1)) != -1)
                    commandParameter.Value = dataRow[commandParameter.ParameterName.Substring(1)];
                ++num;
            }
        }

        private static void AssignParameterValues(
          SqlParameter[] commandParameters,
          object[] parameterValues)
        {
            if (commandParameters == null || parameterValues == null)
                return;
            if (commandParameters.Length != parameterValues.Length)
                throw new ArgumentException("Parameter count does not match Parameter Value count.");
            int index = 0;
            for (int length = commandParameters.Length; index < length; ++index)
            {
                if (parameterValues[index] is IDbDataParameter)
                {
                    IDbDataParameter parameterValue = (IDbDataParameter)parameterValues[index];
                    if (parameterValue.Value == null)
                        commandParameters[index].Value = (object)DBNull.Value;
                    else
                        commandParameters[index].Value = parameterValue.Value;
                }
                else if (parameterValues[index] == null)
                    commandParameters[index].Value = (object)DBNull.Value;
                else
                    commandParameters[index].Value = parameterValues[index];
            }
        }

        private static void PrepareCommand(
          SqlCommand command,
          SqlConnection connection,
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          SqlParameter[] commandParameters,
          out bool mustCloseConnection)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            switch (commandText)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(commandText));
                default:
                    if (connection.State != ConnectionState.Open)
                    {
                        mustCloseConnection = true;
                        connection.Open();
                    }
                    else
                        mustCloseConnection = false;
                    command.Connection = connection;
                    command.CommandText = commandText;
                    if (transaction != null)
                        command.Transaction = transaction.Connection != null ? transaction : throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
                    command.CommandType = commandType;
                    if (commandParameters == null)
                        break;
                    SqlHelper.AttachParameters(command, commandParameters);
                    break;
            }
        }

        public static int ExecuteNonQuery(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteNonQuery(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<int> ExecuteNonQueryAsync(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteNonQueryAsync(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static int ExecuteNonQuery(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        return SqlHelper.ExecuteNonQuery(connection, commandType, commandText, commandParameters);
                    }
            }
        }

        public static async Task<int> ExecuteNonQueryAsync(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            int num;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                num = await SqlHelper.ExecuteNonQueryAsync(connection, commandType, commandText, commandParameters);
            }
            return num;
        }

        public static int ExecuteNonQuery(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (parameterValues == null || parameterValues.Length == 0)
                                return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                            return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static async Task<int> ExecuteNonQueryAsync(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteNonQueryAsync(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static int ExecuteNonQuery(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteNonQuery(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<int> ExecuteNonQueryAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteNonQueryAsync(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static int ExecuteNonQuery(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            SqlCommand command = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(command, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);
            int num = command.ExecuteNonQuery();
            command.Parameters.Clear();
            if (mustCloseConnection)
                connection.Close();
            return num;
        }

        public static async Task<int> ExecuteNonQueryAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(cmd, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);
            int num = await cmd.ExecuteNonQueryAsync();
            cmd.Parameters.Clear();
            if (mustCloseConnection)
                connection.Close();
            return num;
        }

        public static int ExecuteNonQuery(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static async Task<int> ExecuteNonQueryAsync(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteNonQueryAsync(connection, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteNonQueryAsync(connection, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static int ExecuteNonQuery(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteNonQuery(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<int> ExecuteNonQueryAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteNonQueryAsync(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static int ExecuteNonQuery(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            SqlCommand command = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(command, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            int num = command.ExecuteNonQuery();
            command.Parameters.Clear();
            return num;
        }

        public static async Task<int> ExecuteNonQueryAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            int num = await cmd.ExecuteNonQueryAsync();
            cmd.Parameters.Clear();
            return num;
        }

        public static int ExecuteNonQuery(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static async Task<int> ExecuteNonQueryAsync(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteNonQueryAsync(transaction, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static DataSet ExecuteDataset(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteDataset(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteDatasetAsync(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static DataSet ExecuteDataset(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        return SqlHelper.ExecuteDataset(connection, commandType, commandText, commandParameters);
                    }
            }
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            DataSet dataSet;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                dataSet = await SqlHelper.ExecuteDatasetAsync(connection, commandType, commandText, commandParameters);
            }
            return dataSet;
        }

        public static DataSet ExecuteDataset(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (parameterValues == null || parameterValues.Length == 0)
                                return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                            return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteDatasetAsync(connectionString, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteDatasetAsync(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static DataSet ExecuteDataset(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteDataset(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteDatasetAsync(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static DataSet ExecuteDataset(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            SqlCommand sqlCommand = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(sqlCommand, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
            {
                DataSet dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                sqlCommand.Parameters.Clear();
                if (mustCloseConnection)
                    connection.Close();
                return dataSet;
            }
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            SqlCommand sqlCommand = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(sqlCommand, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);
            DataSet dataSet1;
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
            {
                DataSet dataSet2 = new DataSet();
                sqlDataAdapter.Fill(dataSet2);
                sqlCommand.Parameters.Clear();
                if (mustCloseConnection)
                    connection.Close();
                dataSet1 = dataSet2;
            }
            return dataSet1;
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteDatasetAsync(connection, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static DataSet ExecuteDataset(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteDataset(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteDatasetAsync(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static DataSet ExecuteDataset(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            SqlCommand sqlCommand = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(sqlCommand, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
            {
                DataSet dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                sqlCommand.Parameters.Clear();
                return dataSet;
            }
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            SqlCommand sqlCommand = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(sqlCommand, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            DataSet dataSet1;
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
            {
                DataSet dataSet2 = new DataSet();
                sqlDataAdapter.Fill(dataSet2);
                sqlCommand.Parameters.Clear();
                dataSet1 = dataSet2;
            }
            return dataSet1;
        }

        public static async Task<DataSet> ExecuteDatasetAsync(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteDatasetAsync(transaction, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteDatasetAsync(transaction, CommandType.StoredProcedure, spName, spParameterSet);
        }

        private static SqlDataReader ExecuteReader(
          SqlConnection connection,
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          SqlParameter[] commandParameters,
          SqlHelper.SqlConnectionOwnership connectionOwnership)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            bool mustCloseConnection = false;
            SqlCommand command = new SqlCommand();
            command.CommandTimeout = SqlHelper.CommandTimeOut;
            try
            {
                SqlHelper.PrepareCommand(command, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
                SqlDataReader sqlDataReader = connectionOwnership != SqlHelper.SqlConnectionOwnership.External ? command.ExecuteReader(CommandBehavior.CloseConnection) : command.ExecuteReader();
                bool flag = true;
                foreach (DbParameter parameter in (DbParameterCollection)command.Parameters)
                {
                    if (parameter.Direction != ParameterDirection.Input)
                        flag = false;
                }
                if (flag)
                    command.Parameters.Clear();
                return sqlDataReader;
            }
            catch
            {
                if (mustCloseConnection)
                    connection.Close();
                throw;
            }
        }

        private static async Task<SqlDataReader> ExecuteReaderAsync(
          SqlConnection connection,
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          SqlParameter[] commandParameters,
          SqlHelper.SqlConnectionOwnership connectionOwnership)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            bool mustCloseConnection = false;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = SqlHelper.CommandTimeOut;
            SqlDataReader sqlDataReader1;
            try
            {
                SqlHelper.PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
                SqlDataReader sqlDataReader2;
                if (connectionOwnership == SqlHelper.SqlConnectionOwnership.External)
                    sqlDataReader2 = await cmd.ExecuteReaderAsync();
                else
                    sqlDataReader2 = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);
                bool flag = true;
                foreach (DbParameter parameter in (DbParameterCollection)cmd.Parameters)
                {
                    if (parameter.Direction != ParameterDirection.Input)
                        flag = false;
                }
                if (flag)
                    cmd.Parameters.Clear();
                sqlDataReader1 = sqlDataReader2;
            }
            catch
            {
                if (mustCloseConnection)
                    connection.Close();
                throw;
            }
            return sqlDataReader1;
        }

        public static SqlDataReader ExecuteReader(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteReader(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteReaderAsync(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static SqlDataReader ExecuteReader(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    SqlConnection connection = (SqlConnection)null;
                    try
                    {
                        connection = new SqlConnection(connectionString);
                        connection.Open();
                        return SqlHelper.ExecuteReader(connection, (SqlTransaction)null, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.Internal);
                    }
                    catch
                    {
                        connection?.Close();
                        throw;
                    }
            }
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            SqlConnection connection = (SqlConnection)null;
            SqlDataReader sqlDataReader;
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                sqlDataReader = await SqlHelper.ExecuteReaderAsync(connection, (SqlTransaction)null, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.Internal);
            }
            catch
            {
                connection?.Close();
                throw;
            }
            return sqlDataReader;
        }

        public static SqlDataReader ExecuteReader(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (parameterValues == null || parameterValues.Length == 0)
                                return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                            return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteReaderAsync(connectionString, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteReaderAsync(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static SqlDataReader ExecuteReader(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteReader(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteReaderAsync(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static SqlDataReader ExecuteReader(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteReader(connection, (SqlTransaction)null, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.External);
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            return await SqlHelper.ExecuteReaderAsync(connection, (SqlTransaction)null, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.External);
        }

        public static SqlDataReader ExecuteReader(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteReaderAsync(connection, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteReaderAsync(connection, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static SqlDataReader ExecuteReader(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteReader(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteReaderAsync(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static SqlDataReader ExecuteReader(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            return SqlHelper.ExecuteReader(transaction.Connection, transaction, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.External);
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            return await SqlHelper.ExecuteReaderAsync(transaction.Connection, transaction, commandType, commandText, commandParameters, SqlHelper.SqlConnectionOwnership.External);
        }

        public static SqlDataReader ExecuteReader(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static async Task<SqlDataReader> ExecuteReaderAsync(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteReaderAsync(transaction, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteReaderAsync(transaction, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static object ExecuteScalar(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteScalar(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<object> ExecuteScalarAsync(
          string connectionString,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteScalarAsync(connectionString, commandType, commandText, (SqlParameter[])null);
        }

        public static object ExecuteScalar(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        return SqlHelper.ExecuteScalar(connection, commandType, commandText, commandParameters);
                    }
            }
        }

        public static async Task<object> ExecuteScalarAsync(
          string connectionString,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            object obj;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                obj = await SqlHelper.ExecuteScalarAsync(connection, commandType, commandText, commandParameters);
            }
            return obj;
        }

        public static object ExecuteScalar(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (parameterValues == null || parameterValues.Length == 0)
                                return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                            return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static async Task<object> ExecuteScalarAsync(
          string connectionString,
          string spName,
          params object[] parameterValues)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException(nameof(connectionString));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteScalarAsync(connectionString, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteScalarAsync(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static object ExecuteScalar(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteScalar(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<object> ExecuteScalarAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteScalarAsync(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static object ExecuteScalar(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            SqlCommand command = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(command, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);
            object obj = command.ExecuteScalar();
            command.Parameters.Clear();
            if (mustCloseConnection)
                connection.Close();
            return obj;
        }

        public static async Task<object> ExecuteScalarAsync(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(cmd, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);
            object obj = await cmd.ExecuteScalarAsync();
            cmd.Parameters.Clear();
            if (mustCloseConnection)
                connection.Close();
            return obj;
        }

        public static object ExecuteScalar(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static async Task<object> ExecuteScalarAsync(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteScalarAsync(connection, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return await SqlHelper.ExecuteScalarAsync(connection, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static object ExecuteScalar(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteScalar(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static async Task<object> ExecuteScalarAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return await SqlHelper.ExecuteScalarAsync(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static object ExecuteScalar(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            SqlCommand command = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(command, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            object obj = command.ExecuteScalar();
            command.Parameters.Clear();
            return obj;
        }

        public static async Task<object> ExecuteScalarAsync(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            object obj = await cmd.ExecuteScalarAsync();
            cmd.Parameters.Clear();
            return obj;
        }

        public static object ExecuteScalar(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static async Task<object> ExecuteScalarAsync(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            if (spName == null || spName.Length == 0)
                throw new ArgumentNullException(nameof(spName));
            if (parameterValues == null || parameterValues.Length == 0)
                return await SqlHelper.ExecuteScalarAsync(transaction, CommandType.StoredProcedure, spName);
            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
            SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
            return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName, spParameterSet);
        }

        public static XmlReader ExecuteXmlReader(
          SqlConnection connection,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteXmlReader(connection, commandType, commandText, (SqlParameter[])null);
        }

        public static XmlReader ExecuteXmlReader(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            bool mustCloseConnection = false;
            SqlCommand command = new SqlCommand();
            try
            {
                SqlHelper.PrepareCommand(command, connection, (SqlTransaction)null, commandType, commandText, commandParameters, out mustCloseConnection);
                XmlReader xmlReader = command.ExecuteXmlReader();
                command.Parameters.Clear();
                return xmlReader;
            }
            catch
            {
                if (mustCloseConnection)
                    connection.Close();
                throw;
            }
        }

        public static XmlReader ExecuteXmlReader(
          SqlConnection connection,
          string spName,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static XmlReader ExecuteXmlReader(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText)
        {
            return SqlHelper.ExecuteXmlReader(transaction, commandType, commandText, (SqlParameter[])null);
        }

        public static XmlReader ExecuteXmlReader(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            SqlCommand command = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(command, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            XmlReader xmlReader = command.ExecuteXmlReader();
            command.Parameters.Clear();
            return xmlReader;
        }

        public static XmlReader ExecuteXmlReader(
          SqlTransaction transaction,
          string spName,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues == null || parameterValues.Length == 0)
                        return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                    return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static void FillDataset(
          string connectionString,
          CommandType commandType,
          string commandText,
          DataSet dataSet,
          string[] tableNames)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    if (dataSet == null)
                        throw new ArgumentNullException(nameof(dataSet));
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlHelper.FillDataset(connection, commandType, commandText, dataSet, tableNames);
                        break;
                    }
            }
        }

        public static void FillDataset(
          string connectionString,
          CommandType commandType,
          string commandText,
          DataSet dataSet,
          string[] tableNames,
          params SqlParameter[] commandParameters)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    if (dataSet == null)
                        throw new ArgumentNullException(nameof(dataSet));
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlHelper.FillDataset(connection, commandType, commandText, dataSet, tableNames, commandParameters);
                        break;
                    }
            }
        }

        public static void FillDataset(
          string connectionString,
          string spName,
          DataSet dataSet,
          string[] tableNames,
          params object[] parameterValues)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    if (dataSet == null)
                        throw new ArgumentNullException(nameof(dataSet));
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlHelper.FillDataset(connection, spName, dataSet, tableNames, parameterValues);
                        break;
                    }
            }
        }

        public static void FillDataset(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          DataSet dataSet,
          string[] tableNames)
        {
            SqlHelper.FillDataset(connection, commandType, commandText, dataSet, tableNames, (SqlParameter[])null);
        }

        public static void FillDataset(
          SqlConnection connection,
          CommandType commandType,
          string commandText,
          DataSet dataSet,
          string[] tableNames,
          params SqlParameter[] commandParameters)
        {
            SqlHelper.FillDataset(connection, (SqlTransaction)null, commandType, commandText, dataSet, tableNames, commandParameters);
        }

        public static void FillDataset(
          SqlConnection connection,
          string spName,
          DataSet dataSet,
          string[] tableNames,
          params object[] parameterValues)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (dataSet == null)
                throw new ArgumentNullException(nameof(dataSet));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues != null && parameterValues.Length != 0)
                    {
                        SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                        SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                        SqlHelper.FillDataset(connection, CommandType.StoredProcedure, spName, dataSet, tableNames, spParameterSet);
                        break;
                    }
                    SqlHelper.FillDataset(connection, CommandType.StoredProcedure, spName, dataSet, tableNames);
                    break;
            }
        }

        public static void FillDataset(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          DataSet dataSet,
          string[] tableNames)
        {
            SqlHelper.FillDataset(transaction, commandType, commandText, dataSet, tableNames, (SqlParameter[])null);
        }

        public static void FillDataset(
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          DataSet dataSet,
          string[] tableNames,
          params SqlParameter[] commandParameters)
        {
            SqlHelper.FillDataset(transaction.Connection, transaction, commandType, commandText, dataSet, tableNames, commandParameters);
        }

        public static void FillDataset(
          SqlTransaction transaction,
          string spName,
          DataSet dataSet,
          string[] tableNames,
          params object[] parameterValues)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            if (dataSet == null)
                throw new ArgumentNullException(nameof(dataSet));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (parameterValues != null && parameterValues.Length != 0)
                    {
                        SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                        SqlHelper.AssignParameterValues(spParameterSet, parameterValues);
                        SqlHelper.FillDataset(transaction, CommandType.StoredProcedure, spName, dataSet, tableNames, spParameterSet);
                        break;
                    }
                    SqlHelper.FillDataset(transaction, CommandType.StoredProcedure, spName, dataSet, tableNames);
                    break;
            }
        }

        private static void FillDataset(
          SqlConnection connection,
          SqlTransaction transaction,
          CommandType commandType,
          string commandText,
          DataSet dataSet,
          string[] tableNames,
          params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (dataSet == null)
                throw new ArgumentNullException(nameof(dataSet));
            SqlCommand sqlCommand = new SqlCommand();
            bool mustCloseConnection = false;
            SqlHelper.PrepareCommand(sqlCommand, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
            {
                if (tableNames != null && tableNames.Length != 0)
                {
                    string sourceTable = "Table";
                    for (int index = 0; index < tableNames.Length; ++index)
                    {
                        if (tableNames[index] == null || tableNames[index].Length == 0)
                            throw new ArgumentException("The tableNames parameter must contain a list of tables, a value was provided as null or empty string.", nameof(tableNames));
                        sqlDataAdapter.TableMappings.Add(sourceTable, tableNames[index]);
                        sourceTable += (index + 1).ToString();
                    }
                }
                sqlDataAdapter.Fill(dataSet);
                sqlCommand.Parameters.Clear();
            }
            if (!mustCloseConnection)
                return;
            connection.Close();
        }

        public static void UpdateDataset(
          SqlCommand insertCommand,
          SqlCommand deleteCommand,
          SqlCommand updateCommand,
          DataSet dataSet,
          string tableName)
        {
            if (insertCommand == null)
                throw new ArgumentNullException(nameof(insertCommand));
            if (deleteCommand == null)
                throw new ArgumentNullException(nameof(deleteCommand));
            if (updateCommand == null)
                throw new ArgumentNullException(nameof(updateCommand));
            switch (tableName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(tableName));
                default:
                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                    {
                        sqlDataAdapter.UpdateCommand = updateCommand;
                        sqlDataAdapter.InsertCommand = insertCommand;
                        sqlDataAdapter.DeleteCommand = deleteCommand;
                        sqlDataAdapter.Update(dataSet, tableName);
                        dataSet.AcceptChanges();
                        break;
                    }
            }
        }

        public static SqlCommand CreateCommand(
          SqlConnection connection,
          string spName,
          params string[] sourceColumns)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    SqlCommand command = new SqlCommand(spName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    if (sourceColumns != null && sourceColumns.Length != 0)
                    {
                        SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                        for (int index = 0; index < sourceColumns.Length; ++index)
                            spParameterSet[index].SourceColumn = sourceColumns[index];
                        SqlHelper.AttachParameters(command, spParameterSet);
                    }
                    return command;
            }
        }

        public static int ExecuteNonQueryTypedParams(
          string connectionString,
          string spName,
          DataRow dataRow)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (dataRow == null || dataRow.ItemArray.Length == 0)
                                return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                            return SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static int ExecuteNonQueryTypedParams(
          SqlConnection connection,
          string spName,
          DataRow dataRow)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static int ExecuteNonQueryTypedParams(
          SqlTransaction transaction,
          string spName,
          DataRow dataRow)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static DataSet ExecuteDatasetTypedParams(
          string connectionString,
          string spName,
          DataRow dataRow)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (dataRow == null || dataRow.ItemArray.Length == 0)
                                return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                            return SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static DataSet ExecuteDatasetTypedParams(
          SqlConnection connection,
          string spName,
          DataRow dataRow)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static DataSet ExecuteDatasetTypedParams(
          SqlTransaction transaction,
          string spName,
          DataRow dataRow)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static SqlDataReader ExecuteReaderTypedParams(
          string connectionString,
          string spName,
          DataRow dataRow)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (dataRow == null || dataRow.ItemArray.Length == 0)
                                return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                            return SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static SqlDataReader ExecuteReaderTypedParams(
          SqlConnection connection,
          string spName,
          DataRow dataRow)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static SqlDataReader ExecuteReaderTypedParams(
          SqlTransaction transaction,
          string spName,
          DataRow dataRow)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static object ExecuteScalarTypedParams(
          string connectionString,
          string spName,
          DataRow dataRow)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            if (dataRow == null || dataRow.ItemArray.Length == 0)
                                return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName);
                            SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connectionString, spName);
                            SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                            return SqlHelper.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, spParameterSet);
                    }
            }
        }

        public static object ExecuteScalarTypedParams(
          SqlConnection connection,
          string spName,
          DataRow dataRow)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static object ExecuteScalarTypedParams(
          SqlTransaction transaction,
          string spName,
          DataRow dataRow)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static XmlReader ExecuteXmlReaderTypedParams(
          SqlConnection connection,
          string spName,
          DataRow dataRow)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteXmlReader(connection, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        public static XmlReader ExecuteXmlReaderTypedParams(
          SqlTransaction transaction,
          string spName,
          DataRow dataRow)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", nameof(transaction));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    if (dataRow == null || dataRow.ItemArray.Length == 0)
                        return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName);
                    SqlParameter[] spParameterSet = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName);
                    SqlHelper.AssignParameterValues(spParameterSet, dataRow);
                    return SqlHelper.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName, spParameterSet);
            }
        }

        private enum SqlConnectionOwnership
        {
            Internal,
            External,
        }
    }
}
