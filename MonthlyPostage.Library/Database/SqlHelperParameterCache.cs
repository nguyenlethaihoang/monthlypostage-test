﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace MonthlyPostage.Library.Database
{
    public sealed class SqlHelperParameterCache
    {
        private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());

        private SqlHelperParameterCache()
        {
        }

        private static SqlParameter[] DiscoverSpParameterSet(
          SqlConnection connection,
          string spName,
          bool includeReturnValueParameter)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    SqlCommand command = new SqlCommand(spName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    SqlCommandBuilder.DeriveParameters(command);
                    connection.Close();
                    if (!includeReturnValueParameter)
                        command.Parameters.RemoveAt(0);
                    SqlParameter[] array = new SqlParameter[command.Parameters.Count];
                    command.Parameters.CopyTo(array, 0);
                    foreach (DbParameter dbParameter in array)
                        dbParameter.Value = (object)DBNull.Value;
                    return array;
            }
        }

        private static SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
        {
            SqlParameter[] sqlParameterArray = new SqlParameter[originalParameters.Length];
            int index = 0;
            for (int length = originalParameters.Length; index < length; ++index)
                sqlParameterArray[index] = (SqlParameter)((ICloneable)originalParameters[index]).Clone();
            return sqlParameterArray;
        }

        public static void CacheParameterSet(
          string connectionString,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (commandText)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(commandText));
                        default:
                            string key = connectionString + ":" + commandText;
                            SqlHelperParameterCache.paramCache[(object)key] = (object)commandParameters;
                            return;
                    }
            }
        }

        public static SqlParameter[] GetCachedParameterSet(
          string connectionString,
          string commandText)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (commandText)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(commandText));
                        default:
                            string key = connectionString + ":" + commandText;
                            return !(SqlHelperParameterCache.paramCache[(object)key] is SqlParameter[] originalParameters) ? (SqlParameter[])null : SqlHelperParameterCache.CloneParameters(originalParameters);
                    }
            }
        }

        public static SqlParameter[] GetSpParameterSet(
          string connectionString,
          string spName)
        {
            return SqlHelperParameterCache.GetSpParameterSet(connectionString, spName, false);
        }

        public static SqlParameter[] GetSpParameterSet(
          string connectionString,
          string spName,
          bool includeReturnValueParameter)
        {
            switch (connectionString)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(connectionString));
                default:
                    switch (spName)
                    {
                        case "":
                        case null:
                            throw new ArgumentNullException(nameof(spName));
                        default:
                            using (SqlConnection connection = new SqlConnection(connectionString))
                                return SqlHelperParameterCache.GetSpParameterSetInternal(connection, spName, includeReturnValueParameter);
                    }
            }
        }

        internal static SqlParameter[] GetSpParameterSet(
          SqlConnection connection,
          string spName)
        {
            return SqlHelperParameterCache.GetSpParameterSet(connection, spName, false);
        }

        internal static SqlParameter[] GetSpParameterSet(
          SqlConnection connection,
          string spName,
          bool includeReturnValueParameter)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            using (SqlConnection connection1 = (SqlConnection)((ICloneable)connection).Clone())
                return SqlHelperParameterCache.GetSpParameterSetInternal(connection1, spName, includeReturnValueParameter);
        }

        private static SqlParameter[] GetSpParameterSetInternal(
          SqlConnection connection,
          string spName,
          bool includeReturnValueParameter)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            switch (spName)
            {
                case "":
                case null:
                    throw new ArgumentNullException(nameof(spName));
                default:
                    string key = connection.ConnectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");
                    if (!(SqlHelperParameterCache.paramCache[(object)key] is SqlParameter[] originalParameters))
                    {
                        SqlParameter[] sqlParameterArray = SqlHelperParameterCache.DiscoverSpParameterSet(connection, spName, includeReturnValueParameter);
                        SqlHelperParameterCache.paramCache[(object)key] = (object)sqlParameterArray;
                        originalParameters = sqlParameterArray;
                    }
                    return SqlHelperParameterCache.CloneParameters(originalParameters);
            }
        }
    }
}
