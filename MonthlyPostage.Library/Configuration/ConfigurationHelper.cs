﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;

namespace MonthlyPostage.Library.Configuration
{
    public class ConfigurationHelper
    {
        public IConfiguration config { get; set; }

        /// <summary>
        /// các key lồng phân biệt qua dấu ":"
        /// vd: Kafka:KafkaBrokerList
        /// </summary>
        public ConfigurationHelper()
        {
            IHostEnvironment env = MyServiceProvider.ServiceProvider.GetRequiredService<IHostEnvironment>();

            config = new ConfigurationBuilder()
             .SetBasePath(env.ContentRootPath)
             .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
             .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
             .AddEnvironmentVariables()
             .Build();
        }
        public ConfigurationHelper(IHostEnvironment env)
        {
            config = new ConfigurationBuilder()
             .SetBasePath(env.ContentRootPath)
             .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
             .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
             .AddEnvironmentVariables()
             .Build();
        }
        public ConfigurationHelper(string path, string filename)
        {

            config = new ConfigurationBuilder()
             .SetBasePath(path)
             .AddJsonFile(filename, optional: true)
             .Build();
        }

        /// <summary>
        /// sử dụng khi khai báo 1 class model optional
        /// vd: SettingPath pathss = new ConfigurationHelper().GetAppSettings<SettingPath>("SettingPath");
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetAppSettings<T>(string key) where T : class, new()
        {
            var appconfig = new ServiceCollection()
             .AddOptions()
             .Configure<T>(config.GetSection(key))
             .BuildServiceProvider()
             .GetService<IOptions<T>>()
             .Value;
            return appconfig;
        }
    }
    // I prefer to put this class alone, but it's more obvious.
    public class MyServiceProvider
    {
        public static IServiceProvider ServiceProvider { get; set; }
    }
}
