﻿using MonthlyPostage.Services.Cookies.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FTelSSO.Auth.Cookies.Business
{
    /*
    /// <summary>
    /// xử lý cookie cho nghiệp vụ switch account
    /// </summary>
    public interface IUserCookieService
    {
        /// <summary>
        /// Thêm username vừa login vào list account trên browser |
        /// 1. get cookie with key="<see cref="AppConfigration.ListUserKeyCookie" />" |
        /// 2. Check list userName in list account => create list if null : add new if not exist userName : override cookie
        /// </summary>
        /// <param name="userName">username login</param>
        void SetListUserKey(string userName);

        /// <summary>
        /// Logout tất cả cookie user
        /// </summary>
        void LogoutListUser();
    }

    public class UserCookieService : IUserCookieService
    {
        private readonly ICookieManager _cookieManager;
        public UserCookieService(ICookieManager cookieManager)
        {
            this._cookieManager = cookieManager;
        }

        public void SetListUserKey(string userName)
        {
            try
            {
                List<UserKey> listUserNameKey = _cookieManager.Get<List<UserKey>>(AppConfigration.ListUserKeyCookie);
                if (listUserNameKey == null || listUserNameKey.Count == 0)
                {
                    listUserNameKey = new List<UserKey>();
                    UserKey userKey = new UserKey() { UserNameKey = userName };
                    listUserNameKey.Add(userKey);
                }
                else
                {
                    if (!listUserNameKey.Any(x => x.UserNameKey == userName))
                    {
                        UserKey userKey = new UserKey() { UserNameKey = userName };
                        listUserNameKey.Add(userKey);
                    }
                    else
                    {
                        UserKey userKey = new UserKey() { UserNameKey = userName };
                        int i = listUserNameKey.FindIndex(x => x.UserNameKey == userName);
                        listUserNameKey[i] = userKey;
                    }
                }
                _cookieManager.Set(AppConfigration.ListUserKeyCookie, listUserNameKey, AppConfigration.TimeExpireCookie);
            }
            catch (Exception ex)
            {
                /// Writelog to Kafka
                _kafka.Produce($"setcookie_error", $"Func SetListUserKey {ex.Message} {ex.StackTrace}");
            }
        }

        public void LogoutListUser()
        {
            try
            {
                List<UserKey> listUserNameKey = _cookieManager.Get<List<UserKey>>(AppConfigration.ListUserKeyCookie);
                if (listUserNameKey == null || listUserNameKey.Count == 0)
                {
                    return;
                }
                else
                {
                    foreach (UserKey userKey in listUserNameKey)
                    {
                        FPTIDCookie userCookie = _cookieManager.Get<FPTIDCookie>(PwdProtect.md5(userKey.UserNameKey));
                        if (userCookie == null) continue;
                        else
                        {
                            userCookie.IsLogout = true;
                            _cookieManager.Set(PwdProtect.md5(userKey.UserNameKey), userCookie, AppConfigration.TimeExpireCookie);
                        }
                    }
                }
                _cookieManager.Set(AppConfigration.ListUserKeyCookie, listUserNameKey, AppConfigration.TimeExpireCookie);
            }
            catch (Exception ex)
            {
                _kafka.Produce($"setcookie_error", $"Func LogoutListUser {ex.Message} {ex.StackTrace}");
            }
        }
    }
    */
}
