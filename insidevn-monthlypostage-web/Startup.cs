﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using MonthlyPostage.Library.Configuration;
using MonthlyPostage.Library.Database;
using Hangfire.Dashboard;
using System.Text;
using Hangfire;
using Hangfire.States;
using Hangfire.Storage;
using MonthlyPostage.Services.Cookies.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
using MonthlyPostage.Data.Postage;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Hangfire.MemoryStorage;
using MonthlyPostage.ManagementDashboard;

namespace MonthlyPostage
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        private readonly IHostEnvironment _env;
        private readonly ConfigurationHelper _configurationHelper;

        public static CalculateProcess _calProcess;
        //public static List<Step> _fsProcess;
       // public static string _tabDefault;


        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
            _configurationHelper = new ConfigurationHelper(_env);

            //get process fromMasterData
            _calProcess = new CalculateProcess();

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();
            #region Hangfire Configuration

            var hangFireRedisConf = _configurationHelper.GetAppSettings<HangFireRedisStorage>("HangFireConfigure:RedisStorage");
            String redisFormat = "{0}:{1},AllowAdmin=true,abortConnect=false,SyncTimeout={2}";
            string redisConfString = string.Format(redisFormat, hangFireRedisConf.DB_HOST, hangFireRedisConf.DB_PORT, hangFireRedisConf.SyncTimeout);

            //services.AddHangfire(configuration => configuration.UseRedisStorage(redisConfString));
            services.AddHangfire(configuration => configuration.UseMemoryStorage());


            //thoi gian cho giua nhieu job khac nhau
            //Add the processing server as IHostedService
            services.AddHangfireServer(option =>
            {
                option.SchedulePollingInterval = TimeSpan.Zero;
            });
            #endregion

            #region authenticate
            services.AddAuthentication(
                option =>
                {
                    option.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    option.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                }
            );
            #endregion

            #region Cookies CookieManager with options
            services.AddCookieManager(options =>
            {
                /// allow cookie data to encrypt by default it allow encryption
                options.AllowEncryption = false;

                /// Throw if not all chunks of a cookie are available on a request for re-assembly.
                options.ThrowForPartialCookies = true;

                /// set null if not allow to devide in chunks
                //options.ChunkSize = null;

                /// Default Cookie expire time if expire time set to null of cookie
                //default time is 60 day to expire cookie 
                options.DefaultExpireTimeInDays = 60;
            });

            /// su dung cai nay vi c dinh toi set cookie de quan ly switch account
            /// Starting from ASP.NET Core 2.1, the templates include a GDPR compliant configuration of your CookiePolicyOptions in Startup.cs
            services.Configure<CookiePolicyOptions>(options =>
            {
                ///The CheckConsentNeeded option of true will prevent any non-essential cookies from being sent to the browser(no Set - Cookie header) without the user's explicit permission.
                /// This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                //options.MinimumSameSitePolicy = SameSiteMode.None; Unspecified
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.Secure = CookieSecurePolicy.Always;
                options.HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.Always;
            });
            #endregion

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IBackgroundJobClient backgroundJobs)
        {
            MyServiceProvider.ServiceProvider = app.ApplicationServices;

            #region HangFireSchedule
            app.UseHangfireDashboard();
            JobHistoryRenderer.Register(SucceededState.StateName, SucceededRenderer);

            app.UseHangfireDashboard("/InsidePostage", new DashboardOptions
            {
                DisplayStorageConnectionString = false,
                Authorization = new[] { new MyAuthorizationFilter() }
            });


            #region Extend Dashboard
           
            DashboardRoutes.Routes.AddRazorPage("/management", x => new MonthlyPostage.ManagementDashboard.ManagementPage());
            NavigationMenu.Items.Add(page => new MenuItem("Management", page.Url.To("/management"))
            {
                Active = page.RequestPath.StartsWith("/management")
            });

            DashboardRoutes.Routes.AddRazorPage("/management/fsafe", x => new MonthlyPostage.ManagementDashboard.FSafeCronJob());
            //DashboardRoutes.Routes.AddRazorPage("/management/fsaferecurring", x => new MonthlyPostage.ManagementDashboard.FSafeRecurringJob());
           
            #endregion


            //Run
            PostageRepository pRepo = new PostageRepository();
            using (var connection = JobStorage.Current.GetConnection())
            {
                foreach (var recurringJob in connection.GetRecurringJobs())
                {
                    RecurringJob.RemoveIfExists(recurringJob.Id);
                }
            }

            RecurringJob.AddOrUpdate("FSafeMonthlyPostage", () => pRepo.FSafePostage(""), Cron.Yearly(12, 31, 12, 59));
            RecurringJob.AddOrUpdate("IPStaticMonthlyPostage", () => pRepo.IPStaticPostage(""), Cron.Yearly(12, 31, 12, 59));
            RecurringJob.AddOrUpdate("MonthlyPostageRecurring3", () => Console.WriteLine("UltraFast"), Cron.Yearly(12, 31, 12, 59));

            //Type thisType = typeof(PostageRepository);
            //MethodInfo theMethod = thisType.GetMethod("IPStaticPostage");
            //theMethod.Invoke(null, null);

            
 
            #endregion

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }


            app.UseStaticFiles();

            app.UseRouting();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapHangfireDashboard();

                endpoints.MapHealthChecks("/health", new HealthCheckOptions() { 
                    ResultStatusCodes =
                    {
                        [HealthStatus.Healthy]= StatusCodes.Status200OK,
                        [HealthStatus.Degraded]= StatusCodes.Status200OK,
                        [HealthStatus.Unhealthy]= StatusCodes.Status503ServiceUnavailable
                    }
                });
            });
        }

        //This function custom dashboard
        public static NonEscapedString SucceededRenderer(HtmlHelper html, IDictionary<string, string> stateData)
        {
            var builder = new StringBuilder();
            builder.Append("<dl class=\"dl-horizontal\">");

            var itemsAdded = false;

            if (stateData.ContainsKey("Latency"))
            {
                var latency = TimeSpan.FromMilliseconds(long.Parse(stateData["Latency"]));

                builder.Append($"<dt>Latency:</dt><dd>{html.ToHumanDuration(latency, false)}</dd>");

                itemsAdded = true;
            }

            if (stateData.ContainsKey("PerformanceDuration"))
            {
                var duration = TimeSpan.FromMilliseconds(long.Parse(stateData["PerformanceDuration"]));
                builder.Append($"<dt>Duration:</dt><dd>{html.ToHumanDuration(duration, false)}</dd>");

                itemsAdded = true;
            }

            if (stateData.ContainsKey("Result") && !String.IsNullOrWhiteSpace(stateData["Result"]))
            {
                string result = stateData["Result"].ToString();

                var decodeString = System.Net.WebUtility.HtmlDecode(result).Remove(0, 1).Remove(result.Length-2,1);
                builder.AppendFormat("<dt>Result:</dt><dd style='white-space: pre-line;'>{0}</dd>", decodeString);

                itemsAdded = true;
            }

            builder.Append("</dl>");

            if (!itemsAdded) return null;

            return new NonEscapedString(builder.ToString());
        }

        //This function set role using dashboard from client
        public class MyAuthorizationFilter : IDashboardAuthorizationFilter
        {
            public bool Authorize(DashboardContext context)
            {
                // In case you need an OWIN context, use the next line, `OwinContext` class
                // is the part of the `Microsoft.Owin` package.
                //var owinContext = new OwinContext(context.GetOwinEnvironment());

                // Allow all authenticated users to see the Dashboard (potentially dangerous).
                //return owinContext.Authentication.User.Identity.IsAuthenticated;
                return true;
            }
        }


        
    }
}
