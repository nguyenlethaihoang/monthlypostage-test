﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MonthlyPostage.Models;
using MonthlyPostage.Services.Cookies.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MonthlyPostage.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        //private readonly CustomerRepository _customerRepository;
        private readonly ICookieManager _cookieManager;

        public HomeController(ILogger<HomeController> logger, ICookieManager cookieManager)
        {
            _logger = logger;
            //_customerRepository = new CustomerRepository("SQLConnectionRead");
            _cookieManager = cookieManager;
        }

        public IActionResult Index()
        {
            _cookieManager.Set("KeyCookie", "TestValues");
            //List<CustomerInfoModel> list = await _customerRepository.GetCustomerInfo();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
