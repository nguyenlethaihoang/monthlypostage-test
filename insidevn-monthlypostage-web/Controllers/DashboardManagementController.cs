﻿using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MonthlyPostage.Data.Postage;
using MonthlyPostage.ManagementDashboard;
using MonthlyPostage.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MonthlyPostage.Controllers
{
    public class DashboardManagementController : Controller
    {
        private PostageRepository pRepo = new PostageRepository();
        private FSafeRepository fRepo = new FSafeRepository();
        private IPStaticRepository ipRepo = new IPStaticRepository();
        //public CalculateProcess calculateProcess = new CalculateProcess();

        private readonly ILogger<DashboardManagementController> _logger;
        public DashboardManagementController(ILogger<DashboardManagementController> logger)
        {
            _logger = logger;
        }

        //Create and run Fire and Forget Job
        public dynamic TriggerFFJob(string[] excuteParam)
        {
            try
            {
                //Get process of service
                List<Step> process = null;
                string service = excuteParam[excuteParam.Length - 1].Split('|')[0].ToUpper();

                switch (service)
                {
                    case "FSAFE":
                        {
                            MonthlyPostage.Startup._calProcess._tabDefault = "btntabProcess";
                            process = MonthlyPostage.Startup._calProcess._faseProcess;
                            break;
                        }
                       
                }

                if (process != null)
                {
                    //GeneralFunc.ClearOldResult(MonthlyPostage.Startup._fsProcess);

                    foreach (var step in excuteParam)
                    {
                        if (step == "on")
                            continue;

                        //excuteParam[i]: 'FSafe|1'--> service|StepID
                        //get stepID --> create and excute FFJob
                        int stepID = int.Parse(step.Split('|')[1].ToString());

                        Step item = process.First(i => i.id == stepID);
                        var jobId = BackgroundJob.Enqueue(() => Excute(item.excuteName, item.param, item.service));

                        //update result to _faseProcess
                        MonthlyPostage.Startup._calProcess._faseProcess = MonthlyPostage.Startup._calProcess._faseProcess.Select(n =>
                        {
                            if (n.id == stepID) { n.jobID = jobId; n.state = ""; }
                            return n;
                        }).ToList();
                    }

                    UpdateJobResult();

                }

            }
            catch (Exception ex)
            {
                string E = ex.ToString();
            }
            return MonthlyPostage.Startup._calProcess._faseProcess;
        }


        public object Excute(string functionName, object[] objectParam, string service)
        {

            Type type = null;
            switch (service.ToUpper())
            {
                case "FSAFE":
                    type = typeof(MonthlyPostage.Data.Postage.FSafeRepository);
                    break;
                case "IPSTATIC":
                    type = typeof(MonthlyPostage.Data.Postage.IPStaticRepository);
                    break;
                case "POSTAGE":
                    type = typeof(MonthlyPostage.Data.Postage.PostageRepository);
                    break;
            }

            if (type != null)
            {
                //Type type = typeof(MonthlyPostage.Data.Postage.FSafeRepository);
                object instance = Activator.CreateInstance(type);
                MethodInfo method = type.GetMethod(functionName);
                method.Invoke(instance, objectParam);
            }

            return "Excute " + functionName + " successs";

        }


        public object UpdateJobResult()
        {
            int waitTime = 1;
            using (var connection = JobStorage.Current.GetConnection())
            {
                foreach (var step in MonthlyPostage.Startup._calProcess._faseProcess)
                {
                    if (!string.IsNullOrWhiteSpace(step.jobID))
                    {
                        Hangfire.Storage.JobData hfJob = connection.GetJobData(step.jobID);
                        
                        while ( hfJob.State.ToUpper() != "SUCCEEDED" &&
                                hfJob.State.ToUpper() != "SCHEDULED" &&
                                hfJob.State.ToUpper() != "FAILED"  && 
                                step.state == "" && waitTime < 6 ) 
                        {
                            waitTime++;
                            System.Threading.Thread.Sleep(1000);

                            hfJob = connection.GetJobData(step.jobID);
                        }

                        //cancel reSchedule
                        if (hfJob.State.ToUpper() == "SCHEDULED")
                            Hangfire.BackgroundJob.Delete(step.jobID);

                        MonthlyPostage.Startup._calProcess._faseProcess.Select(p =>
                        {
                            if (p.jobID == step.jobID)
                            {
                                p.excuteTime = hfJob.CreatedAt.ToLocalTime().ToString();
                                if (hfJob.State.ToUpper()== "SCHEDULED") p.state = "Failed and Deleted"; 
                                else p.state = hfJob.State;
                            }
                            return p;
                        }).ToList();
                        waitTime = 1;
                    }
                }
            }




            return true;
        }

       

    }

    public class GeneralFunc
    {
        public static dynamic ClearOldResult(List<Step> pocess)
        {
            try
            {
                pocess.Select(n =>
                {
                    n.state = "";
                    n.jobID = "";
                    n.result = "";
                    n.excuteTime = "";
                    return n;
                }).ToList();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            return true;
        }
    }

}
