﻿using MonthlyPostage.Library.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Hangfire;
using MonthlyPostage.Library.Configuration;
using Hangfire.Dashboard;
using System.Diagnostics.CodeAnalysis;
using Hangfire.Dashboard.Pages;
using Hangfire.Storage;

namespace MonthlyPostage.ManagementDashboard
{

    public class CalculateProcess
    {
        public List<Step> _faseProcess;
        public string _tabDefault;

        public CalculateProcess()
        {
            _faseProcess = new List<Step>()
            {
                //Move to DB !
                new Step(){ id = 1, stepName = "Add Real Prepaid", excuteName = "FSafe_AddRealPrepaid"},
                new Step(){ id = 2, stepName = "Get Customer", excuteName = "FSafe_GetCustomer"},
                new Step(){ id = 3, stepName = "Calculate Free Month", excuteName = "FSafe_CalFreeMonth"},
                new Step(){ id = 4, stepName = "Calculate Payment Fee", excuteName = "FSafe_CalPaymentFee"},
                new Step(){ id = 5, stepName = "Subtract Real Prepaid", excuteName = "FSafe_SubtractRealPrepaid"},
                new Step(){ id = 6, stepName = "Calculate Billing Fee", excuteName = "FSafe_CalBillingFee" }

            };
            _faseProcess.Select(c => { c.service = "Fsafe"; return c; }).ToList();

            _tabDefault = "tabDefault";
        }
    }



    public class Step
    {
        public int id { get; set; }
        public string stepName { get; set; }

        public string excuteName { get; set; } //functionName
        public string service { get; set; }
        public object[] param { get; set; }

        public string result { get; set; }
        public string state { get; set; }
        public string jobID { get; set; }
        public string excuteTime { get; set; } //'dd-MM-yyyy'
    }

    public class TypeAction
    {
        public string tabDefault { get; set; }
    }

}
