﻿using MonthlyPostage.Library.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Hangfire;
using MonthlyPostage.Library.Configuration;
using Hangfire.Dashboard;
using System.Diagnostics.CodeAnalysis;
using Hangfire.Dashboard.Pages;

namespace MonthlyPostage.ManagementDashboard
{
    public class ManagementPage : RazorPage
    {
        
        public override void Execute()
        {
            ReSetupSubPage();

            WriteLiteral("\r\n");
            Layout = new LayoutPage("Management");

            WriteLiteral("<div class=\"row\">\r\n");
            WriteLiteral("<div class=\"col-md-3\">\r\n");

            Write(Html.RenderPartial(new CustomSidebarMenu(ManagementSidebarMenu.Items)));

            WriteLiteral("</div>\r\n");

            WriteLiteral("<div class=\"col-md-9\">\r\n");
            WriteLiteral("<h1 class=\"page-header\">\r\n");
            Write("Management");
            WriteLiteral("</h1>\r\n");

            WriteLiteral("<div class=\"alert alert-success\">\r\n");
            Write("Please choose your service.");
            WriteLiteral("\r\n</div>\r\n");
            WriteLiteral("\r\n</div>\r\n");
        }

        public void ReSetupSubPage()
        {
            MonthlyPostage.Startup._calProcess._tabDefault = "tabDefault";
        }

    }

    public class CustomSidebarMenu : RazorPage
    {
        public CustomSidebarMenu([NotNull] IEnumerable<Func<RazorPage, MenuItem>> items)
        {
            if (items == null) throw new ArgumentNullException(nameof(items));
            Items = items;
        }

        public IEnumerable<Func<RazorPage, MenuItem>> Items { get; }

        public override void Execute()
        {
            
            WriteLiteral("\r\n");

            if (!Items.Any()) return;

            WriteLiteral("<div id=\"stats\" class=\"list-group\">\r\n");

            foreach (var item in Items)
            {
                var itemValue = item(this);
                WriteLiteral("<a href=\"");
                Write(itemValue.Url);
                WriteLiteral("\" class=\"list-group-item ");
                Write(itemValue.Active ? "active" : null);
                WriteLiteral("\">\r\n");
                Write(itemValue.Text);
                WriteLiteral("\r\n<span class=\"pull-right\">\r\n");

                foreach (var metric in itemValue.GetAllMetrics())
                {
                    Write(Html.InlineMetric(metric));
                }

                WriteLiteral("</span>\r\n</a>\r\n");
            }

            WriteLiteral(documentReady());

            WriteLiteral("</div>\r\n");
        }

        public string documentReady()
        {
            string result = "";
            string tabID = MonthlyPostage.Startup._calProcess._tabDefault;

            result = $@"
            <script>
            document.addEventListener(""DOMContentLoaded"", function(event) {{
                debugger;
                var tabDefault = document.getElementById('{tabID}');

                if(tabDefault != null && tabDefault != undefined)
                    tabDefault.click();
            document.getElementById('footer').innerHTML = '';
            
            }});
            
            </script>


            ";


            return result;
        }
    }


    public static class ManagementSidebarMenu
    {
        public static readonly List<Func<RazorPage, MenuItem>> Items
            = new List<Func<RazorPage, MenuItem>>();

        static ManagementSidebarMenu()
        {
            Items.Add(page => new MenuItem("FSafe", page.Url.To("/management/fsafe"))
            {
                Active = page.RequestPath.StartsWith("/management/fsafe")
            });

            Items.Add(page => new MenuItem("IPStatic", page.Url.To("/management/ipstatic"))
            {
                Active = page.RequestPath.StartsWith("/management/IPStatic")
            });

            Items.Add(page => new MenuItem("Ultra Fast", page.Url.To("/management/ultrafast"))
            {
                Active = page.RequestPath.StartsWith("/management/ultrafast")
            });

            Items.Add(page => new MenuItem("Plume", page.Url.To("/management/plume"))
            {
                Active = page.RequestPath.StartsWith("/management/plume")
            });

           
        }
    }

    

}
