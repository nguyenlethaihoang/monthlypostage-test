﻿using MonthlyPostage.Library.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Hangfire;
using MonthlyPostage.Library.Configuration;
using Hangfire.Dashboard;
using System.Diagnostics.CodeAnalysis;
using Hangfire.Dashboard.Pages;
using Hangfire.Storage;

namespace MonthlyPostage.ManagementDashboard
{
    public class FSafeCronJob : RazorPage
    {
        //public CalculateProcess calculateProcess;

        public override void Execute()
        {
            WriteLiteral("\r\n");
            Layout = new LayoutPage("Management");

            WriteLiteral("<div class=\"row\">\r\n");
            WriteLiteral("<div class=\"col-md-3\">\r\n");

            Write(Html.RenderPartial(new CustomSidebarMenu(ManagementSidebarMenu.Items)));

            WriteLiteral("</div>\r\n");

            WriteLiteral("<div class=\"col-md-9\">\r\n");
            WriteLiteral("<h2 class=\"page-header\">\r\n");
            Write("FSafe Postage Monthly");
            WriteLiteral("</h2>\r\n");

            WriteLiteral(TabMenu());

            //WriteLiteral(FSafeJobConten());
            WriteLiteral(Trigger());
            //WriteLiteral(documentReady());

            WriteLiteral("\r\n</div>\r\n");
        }


        public string TabMenu()
        {
            string result = "";
            string[] contenArray = new string[2] { FSafeJobConten(), "Load Reccuring by ServiceName!" };
            string loadDefault = Startup._calProcess._tabDefault;

            result = $@"
            <style>
            body {{font-family: Arial;}}

            /* Style the tab */
            .tab {{
              overflow: hidden;
              border: 1px solid #ccc;
              background-color: #f1f1f1;
            }}

            /* Style the buttons inside the tab */
            .tab button {{
              background-color: inherit;
              float: left;
              border: none;
              outline: none;
              cursor: pointer;
              padding: 12px 14px;
              transition: 0.3s;
              font-size: 15px;
            }}

            /* Change background color of buttons on hover */
            .tab button:hover {{
              background-color: #ddd;
            }}

            /* Create an active/current tablink class */
            .tab button.active {{
              background-color: #ccc;
            }}

            /* Style the tab content */
            .tabcontent {{
              display: none;
              padding: 6px 12px;
              border: 1px solid #ccc;
              border-top: none;
            }}
            </style>
            
            <div class=""tab"">
              <button class=""tablinks"" id=""btntabProcess"" onclick=""loadConten(event, 'tabProcess')""  >Process</button>
              <button class=""tablinks"" id=""btntabRecurrring"" onclick=""loadConten(event, 'tabRecurrring')""  >Recurrring Job</button>
            </div>

            <div id=""tabProcess"" class=""tabcontent"">
	            <!--Fsafe cronjob conten-->
              <h3>Process here</h3>
              <p>London is the capital city of England.</p>
              {contenArray[0]}
            </div>

            <div id=""tabRecurrring"" class=""tabcontent"">
              <h3>Recurring Job</h3>
              <p>Paris is the capital of France.</p> 
               {contenArray[1]}
            </div>


            <script>
            function loadConten(evt, tabName) {{
              var i, tabcontent, tablinks;
              tabcontent = document.getElementsByClassName(""tabcontent"");
              for (i = 0; i < tabcontent.length; i++) {{
                tabcontent[i].style.display = ""none"";
              }}
              tablinks = document.getElementsByClassName(""tablinks"");
              for (i = 0; i < tablinks.length; i++) {{
                tablinks[i].className = tablinks[i].className.replace("" active"", """");
              }}
              document.getElementById(tabName).style.display = ""block"";
              evt.currentTarget.className += "" active"";
            }}
            
            
            </script>

            ";
            return result;
        }

        public string FSafeJobConten()
        {
            string result = "";

            List<Step> process = MonthlyPostage.Startup._calProcess._faseProcess;

            result = $@"
            <div class=""alert alert-success"">
            <div class=""row"">
                <div class=""col-md-12"">
        
                    <div class=""js-jobs-list"">
                        <div class=""btn-toolbar btn-toolbar-top"">
                            <button class=""js-jobs-list-command btn btn-sm btn-primary""
                        
                                    data-loading-text=""Triggering...""
                                    disabled=""disabled""
                                    id = ""btnTrigger"">
                                <span class=""glyphicon glyphicon-play-circle""></span>
                                Trigger now
                            </button>
                            <button class=""js-jobs-list-command btn btn-sm btn-default""
                        
                                    data-loading-text=""Deleting...""
                                    data-confirm=""Do you really want to DELETE ALL selected jobs?""
                                    disabled=""disabled"">
                                <span class=""glyphicon glyphicon-remove""></span>
                                Delete
                            </button>
                        </div>

                        <div class=""table-responsive"">
                            <table class=""table"" id=""tb_stepInfo"" >
                                <thead>
                                    <tr>
                                        <th class=""min-width"">
                                            <input type=""checkbox"" class=""js-jobs-list-select-all"" />
                                        </th>
                                        <th>Step Name</th>
                                        <th class=""min-width"">State</th>
                                        <th>Last Execution</th>
                                        <th>Job-Infomation</th>
                                       <!-- <th>Job</th>
                                        <th class=""align-right min-width"">Next execution</th>
                                        <th class=""align-right min-width"">Last execution</th>
                                        <th class=""align-right min-width"">Created</th> -->
                                    </tr>
                                </thead>
                                <tbody>";

            foreach (var step in process)
            {

                result += $@"
                         <tr class=""js-jobs-list-row hover"">
                            <td rowspan=""1"">
                                <input type=""checkbox"" class=""js-jobs-list-checkbox"" name=""jobs[]"" value=""{step.service}|{step.id}"" />
                            </td>
                            <td class=""word-break width-215"">{step.id}. {step.stepName}</td>
                            <td class=""min-width min-width-125p "">
                                {step.state}
                            </td>
                            <td>
                                <!--<span title=""(UTC) Coordinated Universal Time"" data-container=""body"">
                                    UTC
                                </span>-->
                                {step.excuteTime}
                            </td>
                            <td class=""word-break width-30""> 
                                {step.jobID}
                            </td> 
                ";
            }

            result += $@"
                            <!-- <td class=""align-right min-width""> -->
                                <!-- <span data-moment=""1704027540"">31-12-2023 12:59:00</span> -->
                            <!-- </td> -->
                            <!-- <td class=""align-right min-width""> -->
                                <!-- <em>N/A</em> -->
                            <!-- </td> -->
                            <!-- <td class=""align-right min-width""> -->
                                <!-- <span data-moment=""1676794977"">19-02-2023 8:22:57</span> -->
                            <!-- </td> -->
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>

</div>                                   
</div>    
";
            return result;
        }

        public string Trigger()
        {
            string result = "";
            result = $@"
            <script>
            var checkedList = [];
            var tbString = '';
            document.getElementById('btnTrigger').onclick = function()
             {{
                    debugger;
                    var $this = $(this);
                   
                    $(':checkbox:checked').each(function(i){{
                          checkedList[i] = $(this).val();
                        }});   

                    for (let i = 0; i < checkedList.length; ++i) {{
                             alert(checkedList[i] + '!');
                        }}
                
                    debugger;
                  

                    $.ajax({{
                    url: '/DashboardManagement/TriggerFFJob',
                    type: 'POST',
                    async: false,   
                    data: {{
                            excuteParam: checkedList
                          }},
                    success: function (data) {{
                        debugger;       
                        
                        //var _result = JSON.parse(data);
                        var _result = data;
                        var tbStr = ''

                         for (var i = 0; i < _result.length; i++) {{

							 tbStr += '<tr class=""js-jobs-list-row hover""><td rowspan=""1""><input type=""checkbox"" class=""js-jobs-list-checkbox"" name=""jobs[]"" value=""'+
                                                   _result[i].service + '|' + _result[i].id + '"" /></td>';
                             tbStr += '<td class=""word-break width-215"">' + _result[i].id + '. ' +  _result[i].stepName + '</td>';							 
                             tbStr += '<td class=""min-width min-width-125p "">' + _result[i].state + '</td>';
                             tbStr += '<td>' +  _result[i].state + '</td>';
                             tbStr += '<td class=""word-break width-30"">' +  formatdate(_result[i].excuteTime)  +  '</td> </tr>';	

							 /*alert(tbStr);*/
                            
                            }}
                            /*$('#tb_stepInfo').html(LoadContentTable(tbStr));*/
                       
                        
                            }}
                            }});

                }};
				  
            function LoadContentTable(_body) {{
                 var str = '<thead><tr><th class=""min-width""><input type=""checkbox"" class=""js-jobs-list-select-all"" /></th><th>StepName</th><th class=""min-width"">State</th><th>ExcuteTime</th><th>Job-Infomation</th></tr></thead><tbody> '
                            + _body +'</tbody>'
                 return str;
            }};
            
            function formatdate(d) {{
            d = new Date(d);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {{
                day = ""0"" + day;
            }}
            if (month < 10) {{
                month = ""0"" + month;
            }}
            var date = day + "" / "" + month + "" / "" + year;
            return date;
            }};     
            
            </script>

";
            return result;
        }

        
    }




}
