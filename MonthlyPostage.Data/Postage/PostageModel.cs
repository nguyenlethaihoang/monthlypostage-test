﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonthlyPostage.Data.Postage
{
    public class PostageModel
    {

    }
    public class Postage_ReusultModel<T>
    {
        public string Step { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public int Count { get; set; }
        public T Data { get; set; }

        public Postage_ReusultModel(int Code, int count)
        {
            if (Code == 0) { Message = "Fail"; Count = count; }
            if (Code == 1) { Message = "Success"; Count = count; }
        }

        public Postage_ReusultModel(int Code)
        {
            if (Code == 0) { Message = "Fail"; }
            if (Code == 1) { Message = "Success"; }
        }

        public Postage_ReusultModel(int Code, string messageException)
        {
            if (Code < 0)
            {
                Message = "Fail";
                Exception = messageException;
            }
        }

        public Postage_ReusultModel(string message, int count)
        {
            Message = message;
            Count = count;

        }
        public Postage_ReusultModel(string name)
        {
            Step = name;
            Message = "Success";
        }
    }

    public class Postage_AddPrepaidModel
    {
        public DateTime StartDate;
        public DateTime EndDate;
    }

    public class Postage_GetCustomerModel
    {
        public int Type { get; set; } //1: IP, 2:FSafe
        public DateTime BillingDate { get; set; } //Ngay dau thang sau
    }

    public class Postage_CalFreeMonthModel
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
    }

    public class Postage_DiscountComboFee
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class Postage_PromotionFeeModel
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class Postage_DiscountFeeModel
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }

}
