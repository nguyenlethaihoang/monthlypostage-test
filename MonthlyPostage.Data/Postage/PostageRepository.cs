﻿using MonthlyPostage.Library.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Hangfire;
using MonthlyPostage.Library.Configuration;

namespace MonthlyPostage.Data.Postage
{
    public class PostageRepository
    {
        private static ConfigurationHelper _configurationHelper;
        private static FSafeRepository fRepo = new FSafeRepository();
        private static IPStaticRepository ipRepo = new IPStaticRepository();

        public string FSafePostage(string rsString)
        {
            int reTrytimes = 3;
            bool reTryResult = false;
            rsString += "<br><br><b>__ FSAFE MONTHLY POSTAGE __</b>";

            try
            {
                rsString += Postage_FormatLog(Retry(reTrytimes, () => fRepo.FSafe_AddRealPrepaid(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => fRepo.FSafe_GetCustomer(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => fRepo.FSafe_CalFreeMonth(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => fRepo.FSafe_CalPaymentFee(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => fRepo.FSafe_SubtractRealPrepaid(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => fRepo.FSafe_CalBillingFee(), out reTryResult));

                /*
                 TryToDo addRealPrepaid = FSafe_AddRealPrepaid;
                 rsString += Postage_FormatLog(Retry(reTrytimes, addRealPrepaid, out reTryResult));
                 */
            }
            catch (Exception ex) {
                dynamic item = JsonConvert.DeserializeObject<object>(ex.Message.ToString());
                rsString += Postage_FormatLog(item);
               // rsString += "\n" + ex.Message.ToString();
            }

            return rsString;
        }

        public string IPStaticPostage(string rsString)
        {
            int reTrytimes = 3;
            bool reTryResult = false;
            rsString += "<br><br><b>__ IPSTATIC MONTHLY POSTAGE __</b>";

            try
            {
                rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_AddRealPrepaid(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_GetCustomer(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_CalPromotionFee(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_CalFreeMonth(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_CalDiscountFee(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_CalDiscountComboFee(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_CalPaymentFee(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_SubtractRealPrepaid(), out reTryResult));

                if (reTryResult == true)
                    rsString += Postage_FormatLog(Retry(reTrytimes, () => ipRepo.IPStatic_CalBillingFee(), out reTryResult));

            }
            catch (Exception ex) { rsString += "\n" + ex.Message.ToString(); }

            return rsString;
        }


        //1 Recurring Job include 3 service
        public string MonthlyPostageRecurring()
        {
            string rsString = "<b><i>>>>>>> Excute at: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + " <<<<<<</i></b>";

            rsString += FSafePostage(rsString);
            rsString += IPStaticPostage(rsString);

            rsString += "<b><i>>>>>>> Excute end at: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + " <<<<<<</i></b>";

            return Postage_FormatLog(rsString);
        }

        #region General Zone
        public string Postage_FormatLog(Object input)
        {
            string rs = "";
            try
            {
                string jsonStr = JsonConvert.SerializeObject(input);
                jsonStr = jsonStr.Replace("\\\"", "\"");
                jsonStr = jsonStr.Replace("\\n", "<br>");
                jsonStr = jsonStr.Replace(@"{""", " <br>");
                jsonStr = jsonStr.Replace(@"}", " <br>");
                jsonStr = jsonStr.Replace(",", " <br>   - ");
                jsonStr = jsonStr.Replace(@"""", @"");
                jsonStr = jsonStr.Replace(@":", @": ");

                rs = jsonStr;
            }
            catch (Exception ex) { throw ex; }
            return rs;
        }

        public delegate dynamic TryToDo();
        public static dynamic Retry(int reTryTime, TryToDo functionToDo, out bool reTryResult)
        {
            int times = 0;
            dynamic result = functionToDo();
            reTryResult = String.IsNullOrEmpty(result.Exception) ? true : false;

            //ex[0]: 0-catchException, 1-bussinessException
            //ex[1]: message
            //ex[2]: 1-0 retry or notReTry
            string[] ex = (!string.IsNullOrEmpty(result.Exception)) ? result.Exception.Split('|') : new string[] { };

            //retry khi gặp exception
            while (times <= reTryTime && ex.Length > 0)
            {
                //neu catch exception => retry
                //neu bussiness exception va can retry => retry
                if (ex[0] == "0" || (ex[0] == "1" && ex[2] == "1"))
                {
                    result = functionToDo();
                    ex = result.Exception.Split('|');

                    reTryResult = String.IsNullOrEmpty(result.Exception) ? true : false;
                }
                //neu bussiness exception va khong can retry => break
                else if (ex[0] == "1" && ex[2] == "0")
                {
                    reTryResult = String.IsNullOrEmpty(result.Exception) ? true : false;
                    break;
                }

                times++;
            }
            // end retry exception

            result.Step += (times > 0) ? " - has been retry " + times + " times!" : "";
            result.Exception = ex.Length > 0 ? ex[1] : null;
            return result;
        }

        #endregion


       

    }
}
