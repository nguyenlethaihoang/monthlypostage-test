﻿using MonthlyPostage.Library.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Hangfire;
using MonthlyPostage.Library.Configuration;

namespace MonthlyPostage.Data.Postage
{
    public class IPStaticRepository
    {
        private static ConfigurationHelper _configurationHelper;

        #region IPStatic billing monthly step by step
        
        public dynamic IPStatic_AddRealPrepaid()
        {
            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("1. IPStatic Add Real Prepaid ");
            try
            {
                DateTime date = DateTime.Now;
                //Postage_AddPrepaidModel input = new Postage_AddPrepaidModel()
                //{
                //    StartDate = new DateTime(date.Year, date.Month, 1),
                //    EndDate = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)
                //};

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IPStatic_InsertIPPrepaidFromBilling", new object[] { });

                //if (ds != null && ds.Tables.Count > 0)
                //    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                //else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|";
            }

            return result;
        }

        public dynamic IPStatic_GetCustomer()
        {
            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("2. IPStatic Get Customer ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_GetCustomerModel input = new Postage_GetCustomerModel()
                {
                    Type = 1,//1: IP, 2:FSafe
                    BillingDate = new DateTime(date.Year, date.Month, 1).AddMonths(1) //ngay dau thang sau
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_GetCustomer", new object[] { input.Type, input.BillingDate });

                if (ds != null && ds.Tables.Count > 0)
                {
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());

                    //result.Exception = (result.Count == 0) ? "1|Khong phat sinh cuoc FSafe!|0" : "";
                }
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }

        public dynamic IPStatic_CalPromotionFee()
        {

            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("3. IPStatic PromotionFee Calculation ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_PromotionFeeModel input = new Postage_PromotionFeeModel()
                {
                    Month = date.Month - 1,
                    Year = date.Year
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_PromotionFee", new object[] { input.Month, input.Year });

                if (ds != null && ds.Tables.Count > 0)
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }

        public dynamic IPStatic_CalFreeMonth()
        {

            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("4. IPStatic FreeMonth Calculation ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
                {
                    Month = date.Month - 1,
                    Year = date.Year,
                    Type = "IP"//1: FS - IP
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_FreeMonth", new object[] { input.Month, input.Year, input.Type });

                if (ds != null && ds.Tables.Count > 0)
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }

        public dynamic IPStatic_CalDiscountFee()
        {

            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("5. IPStatic DiscountFee Calculation ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_DiscountFeeModel input = new Postage_DiscountFeeModel()
                {
                    Month = date.Month - 1,
                    Year = date.Year
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_DiscountFee", new object[] { input.Month, input.Year });

                if (ds != null && ds.Tables.Count > 0)
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }

        public dynamic IPStatic_CalDiscountComboFee()
        {

            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("6. IPStatic DiscountComboFee Calculation ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_DiscountComboFee input = new Postage_DiscountComboFee()
                {
                    Month = date.Month - 1,
                    Year = date.Year
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_DiscountComboFee", new object[] { input.Month, input.Year });

                if (ds != null && ds.Tables.Count > 0)
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }

        public dynamic IPStatic_CalPaymentFee()
        {

            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("7. IPStatic PaymentFee Calculation ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
                {
                    Month = date.Month - 1,
                    Year = date.Year,
                    Type = "IP"//1: FS - IP
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_PaymentFee", new object[] { input.Month, input.Year, input.Type });

                if (ds != null && ds.Tables.Count > 0)
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }

        public dynamic IPStatic_SubtractRealPrepaid()
        {
            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("8. IPStatic Real Prepaid Substraction ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
                {
                    Month = date.Month - 1,
                    Year = date.Year,
                    Type = "IP"//1: FS - IP
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Insert_RealPrepaid", new object[] { input.Month, input.Year, input.Type });

                if (ds != null && ds.Tables.Count > 0)
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }


        public dynamic IPStatic_CalBillingFee()
        {

            Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("9. IPStatic Billing Fee Calculation ");
            try
            {
                DateTime date = DateTime.Now;
                Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
                {
                    Month = date.Month - 1,
                    Year = date.Year,
                    Type = "IP"//1: FS - IP
                };

                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_BillingFee", new object[] { input.Month, input.Year, input.Type });

                if (ds != null && ds.Tables.Count > 0)
                    result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                else result = new Postage_ReusultModel<dynamic>(0);
            }
            catch (Exception ex)
            {
                result.Exception = "0|" + ex.Message + "|0";
            }

            return result;
        }


        #endregion


    }
}
