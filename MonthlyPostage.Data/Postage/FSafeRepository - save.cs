﻿using MonthlyPostage.Library.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Hangfire;
using MonthlyPostage.Library.Configuration;

namespace MonthlyPostage.Data.Postage
{
    public partial class FSafeRepository
    {
        //private static ConfigurationHelper _configurationHelper;

        #region FSafe billing monthly step by step

        //public void FSafe_AddRealPrepaid(string a)
        //{
        //    Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("1. FSafe Add Real Prepaid ");
        //        DateTime date = DateTime.Now;
        //        Postage_AddPrepaidModel input = new Postage_AddPrepaidModel()
        //        {
        //            StartDate = new DateTime(date.Year, date.Month, 1),
        //            EndDate = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1)
        //        };

        //     DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.FSafe_AddRealPrepaid", new object[] { input.StartDate, input.EndDate });

        //        if (ds != null && ds.Tables.Count > 0)
        //            result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
        //        else result = new Postage_ReusultModel<dynamic>(0);
            
        //}

        //public dynamic FSafe_GetCustomer()
        //{
        //    Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("2. FSafe Get Customer ");
        //    try
        //    {
        //        DateTime date = DateTime.Now;
        //        Postage_GetCustomerModel input = new Postage_GetCustomerModel()
        //        {
        //            Type = 2,//1: IP, 2:FSafe
        //            BillingDate = new DateTime(date.Year, date.Month, 1).AddMonths(1) //ngay dau thang sau
        //        };

        //        DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_GetCustomer", new object[] { input.Type, input.BillingDate });

        //        if (ds != null && ds.Tables.Count > 0)
        //        {
        //            result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());

        //            //result.Exception = (result.Count == 0) ? "1|Khong phat sinh cuoc FSafe!|0" : "";
        //        }
        //        else result = new Postage_ReusultModel<dynamic>(0);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Exception = "0|" + ex.Message + "|0";
        //    }

        //    return result;
        //}

        //public dynamic FSafe_CalFreeMonth()
        //{

        //    Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("3. FSafe FreeMonth Calculation ");
        //    try
        //    {
        //        DateTime date = DateTime.Now;
        //        Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
        //        {
        //            Month = date.Month - 1,
        //            Year = date.Year,
        //            Type = "FS"//1: FS - IP
        //        };

        //        DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_FreeMonth", new object[] { input.Month, input.Year, input.Type });

        //        if (ds != null && ds.Tables.Count > 0)
        //            result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
        //        else result = new Postage_ReusultModel<dynamic>(0);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Exception = "0|" + ex.Message + "|0";
        //    }

        //    return result;
        //}

        //public dynamic FSafe_CalPaymentFee()
        //{

        //    Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("4. FSafe PaymentFee Calculation ");
        //    try
        //    {
        //        DateTime date = DateTime.Now;
        //        Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
        //        {
        //            Month = date.Month - 1,
        //            Year = date.Year,
        //            Type = "FS"//1: FS - IP
        //        };

        //        DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_PaymentFee", new object[] { input.Month, input.Year, input.Type });

        //        if (ds != null && ds.Tables.Count > 0)
        //            result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
        //        else result = new Postage_ReusultModel<dynamic>(0);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Exception = "0|" + ex.Message + "|0";
        //    }

        //    return result;
        //}

        //public dynamic FSafe_SubtractRealPrepaid()
        //{
        //    Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("5. FSafe Real Prepaid Substraction ");
        //    try
        //    {
        //        DateTime date = DateTime.Now;
        //        Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
        //        {
        //            Month = date.Month - 1,
        //            Year = date.Year,
        //            Type = "FS"//1: FS - IP
        //        };

        //        DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Insert_RealPrepaid", new object[] { input.Month, input.Year, input.Type });

        //        if (ds != null && ds.Tables.Count > 0)
        //            result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
        //        else result = new Postage_ReusultModel<dynamic>(0);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Exception = "0|" + ex.Message + "|0";
        //    }

        //    return result;
        //}

        //public dynamic FSafe_CalBillingFee()
        //{

        //    Postage_ReusultModel<dynamic> result = new Postage_ReusultModel<dynamic>("6. FSafe Billing Fee Calculation ");
        //    try
        //    {
        //        DateTime date = DateTime.Now;
        //        Postage_CalFreeMonthModel input = new Postage_CalFreeMonthModel()
        //        {
        //            Month = date.Month - 1,
        //            Year = date.Year,
        //            Type = "FS"//1: FS - IP
        //        };

        //        DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnRead(), "PowerInside.dbo.IP_Calc_BillingFee", new object[] { input.Month, input.Year, input.Type });

        //        if (ds != null && ds.Tables.Count > 0)
        //            result.Count = int.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
        //        else result = new Postage_ReusultModel<dynamic>(0);
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Exception = "0|" + ex.Message + "|0";
        //    }

        //    return result;
        //}

        #endregion


    }
}
