﻿using MonthlyPostage.Library.Configuration;

namespace MonthlyPostage.Data.ConstDefine
{
    /// <summary>
    /// các thông tin về hằng số, link apis, bala
    /// khai báo new class ConfigurationHelper 1 lần
    /// </summary>
    public static class AppConfigration
    {
        private static ConfigurationHelper _configurationHelper = new ConfigurationHelper();
        /// <summary>
        /// Key gen token api
        /// </summary>
        public static string jwtAuthSecret = _configurationHelper.config["JWTAuth:AppSettings:Secret"];
        /// <summary>
        /// config redis caching
        /// </summary>
        public static string redisConfig = _configurationHelper.config["Redis:ConnectionString"];
    }
}
